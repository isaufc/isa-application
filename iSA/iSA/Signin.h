//
//  Signin.h
//  iSA
//
//  Created by Afonso Neto on 13/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Signin : UIViewController <UITextFieldDelegate> {
    IBOutlet UITextField *nickname;
    IBOutlet UITextField *passwd;
}

@property (nonatomic) UIActivityIndicatorView *loadingSpinner;
@property (nonatomic) NSString *redirectTo;

-(IBAction)loginTouched:(id)sender;
-(IBAction)signupTouched:(id)sender;
-(void)showSpinner;

@end
