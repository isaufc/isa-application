//
//  RUService.h
//  iSA
//
//  Created by Daniel Carlos on 20/10/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainMealModel.h"
#import "BreakFastModel.h"



@interface RUService : NSObject

-(BreakFastModel *) getBreakfastToday;
-(MainMealModel *) getLunchToday;
-(MainMealModel *) getDinnerToday;

@end
