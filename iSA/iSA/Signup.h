//
//  Signup.h
//  iSA
//
//  Created by Afonso Neto on 13/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Signup : UIViewController <UITextFieldDelegate> {
    IBOutlet UITextField *nameField;
    IBOutlet UITextField *nicknameField;
    IBOutlet UITextField *courseField;
    IBOutlet UITextField *passwdField;
    IBOutlet UITextField *passwdConfirmationField;
}

@property (nonatomic) UIActivityIndicatorView *loadingSpinner;
@property (nonatomic) NSString *redirectTo;

- (IBAction)signupTouched:(id)sender;

@end
