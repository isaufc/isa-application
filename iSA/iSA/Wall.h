//
//  Wall.h
//  iSA
//
//  Created by Afonso Neto on 13/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Wall : UIViewController {}

@property (nonatomic, strong) IBOutlet UIScrollView *wallScroll;

- (void) gotoUploadView;

@end
