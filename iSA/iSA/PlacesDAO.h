//
//  PlacesDAO.h
//  iSA
//
//  Created by Afonso Neto on 10/06/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlacesDAO : UIViewController {}

@property (nonatomic) NSString *placeTitle;
@property (nonatomic) NSString *description;
@property (nonatomic) BOOL hasBathroom;
@property (nonatomic) BOOL hasCanteen;
@property (nonatomic) BOOL hasXerox;
@property (nonatomic) BOOL hasWaterDrinker;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

@end
