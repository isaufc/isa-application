//
//  Signup.m
//  iSA
//
//  Created by Afonso Neto on 13/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "Signup.h"
#import "Parse/Parse.h"
#import "Chat.h"
#import "Wall.h"
#import "BackButton.h"

@interface Signup ()

@end

@implementation Signup

@synthesize redirectTo, loadingSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.navigationItem.title = @"Cadastro";
    [super viewDidLoad];
    
    //It tells to the iOS that this class will respond to handle the keyboard
    nameField.delegate = self;
    nicknameField.delegate = self;
    courseField.delegate = self;
    passwdField.delegate = self;
    passwdConfirmationField.delegate = self;
}

//This one is called when the user touches in the button "Return" of the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == nameField) {
        //If user click in return when the name field is selected, jump the cursor to nickname field
        [nicknameField becomeFirstResponder];
        return true;
    } else if (textField == nicknameField) {
        //If user click in return when the nickname field is selected, jump the cursor to course field
        [courseField becomeFirstResponder];
        return true;
    } else if (textField == courseField) {
        //If user click in return when the course field is selected, jump the cursor to passwd field
        [passwdField becomeFirstResponder];
        return true;
    } else if (textField == passwdField) {
        //If user click in return when the passwd field is selected, jump the cursor to passwd Confirm field
        [passwdConfirmationField becomeFirstResponder];
        return true;
    } else if (textField == passwdConfirmationField) {
        //If user click in return when the nickname field is selected, run the signup method
        [self signupTouched:nil];
        return true;
    }
    return false;
}

//This method will be called when the user touches in any place of the application, and we're using to hide the keyboard when the user touches in a place of app that doesn't make any action
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [nameField resignFirstResponder];
    [nicknameField resignFirstResponder];
    [courseField resignFirstResponder];
    [passwdConfirmationField resignFirstResponder];
    [passwdField resignFirstResponder];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signupTouched:(id)sender {
    //If some of the fields is empty, show a error message
    if ([nameField.text isEqualToString:@""] || [nicknameField.text isEqualToString:@""] || [courseField.text isEqualToString:@""] || [passwdField.text isEqualToString:@""] || [passwdConfirmationField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Verifique o preenchimento correto de todos os campos." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    } else {
        //If the fields are fully filled, verify if the passwd and passwdConfirmation matches, if it doesn't, show a error message
        if (![passwdField.text isEqualToString:passwdConfirmationField.text]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Senha e Confirmação de senha não conferem." delegate:self cancelButtonTitle:@"Vou corrigir." otherButtonTitles:nil,nil];
            [alert show];
        } else {
            //All data correct, so let's create this user
            [self showSpinner];
            PFUser *newUser = [PFUser user];
            newUser.username = nicknameField.text;
            newUser.password = passwdField.text;
            [newUser setObject:nameField.text forKey:@"name"];
            [newUser setObject:courseField.text forKey:@"course"];
            //Try to login the user, if succesfull alert him and sends him to the funcionality he wishes
            [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sucesso" message:@"Seu cadastro foi realizado com sucesso. Utilize o apelido e senha definidos para acesso futuro." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
                    [alert show];
                    if ([self.redirectTo isEqualToString:@"Wall"]) {
                        Wall *wallView = [[Wall alloc] init];
                        [self.navigationController pushViewController:wallView animated:true];
                    } else if ([self.redirectTo isEqualToString:@"Chat"]) {
                        Chat *chatView = [[Chat alloc] init];
                        [self.navigationController pushViewController:chatView animated:true];
                    }
                } else {
                    [self.loadingSpinner removeFromSuperview];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível concluir seu cadastro, verifique os dados digitados (tente usar outro apelido) e sua conexão com a internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
                    [alert show];
                }
             }];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [self setUpImageBackButton];
}

-(void)showSpinner {
    self.loadingSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.loadingSpinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)];
    [self.loadingSpinner startAnimating];
    [self.view addSubview:self.loadingSpinner];
}

@end
