//
//  Spotted.h
//  iSA
//
//  Created by Afonso Neto on 13/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface Spotted : UIViewController <UIWebViewDelegate> {}

@property (nonatomic) UIWebView *webView;

@end
