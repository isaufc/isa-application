//
//  RUService.m
//  iSA
//
//  Created by Daniel Carlos on 20/10/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "RUService.h"
#import "HttpHelper.h"
#import "SMXMLDocument.h"
#import "MainMealModel.h"
#import "BreakFastModel.h"
#import "RUAuxiliar.h"

@implementation RUService{
    NSMutableArray *refeicoes;
}

- (NSMutableArray *)getRefeicoes{
    
    NSString *url = @"http://wservice.devlooping.com.br/RU/refeicoes";
    HttpHelper *http = [[HttpHelper alloc]init];
    NSData *data = [http doGet:url];
    
    if(!data || [data length]  == 0) {
        NSLog(@"Nenhum dado encontrado");
        return nil;
    }
    
    
    NSError *error;
    SMXMLDocument *document = [SMXMLDocument documentWithData:data error:&error];
    
    if (error) {
        NSLog(@"Error while parsing the document: %@", error);
        return nil;
    }
    
    
    //  NSLog(@"Document:\n %@", document);
    
   refeicoes = [[NSMutableArray alloc] init];
    
    
    NSArray *tagDesjejums = [document.root children];
    
    for (int i = 0; i<[tagDesjejums count]; i++) {
        SMXMLElement *tagDesjejum = (SMXMLElement *)[tagDesjejums objectAtIndex:i];
        
        tagDesjejum = [tagDesjejum childNamed:
                       @"desjejum"];
        
        BreakFastModel *desjejum = [[BreakFastModel alloc]init];
        desjejum.bebidas = [tagDesjejum valueWithPath:@"bebidas"];
        desjejum.especial = [tagDesjejum valueWithPath:@"especial"];
        desjejum.frutas = [tagDesjejum valueWithPath:@"frutas"];
        desjejum.paes = [tagDesjejum valueWithPath:@"paes"];
        
        [refeicoes addObject:desjejum];

    }    
    NSArray *tagAlmocos = [document.root children];
    
    for (int i = 0; i<[tagAlmocos count]; i++) {
        SMXMLElement *tagAlmoco = (SMXMLElement *)[tagAlmocos objectAtIndex:i];
        
        tagAlmoco = [tagAlmoco childNamed:
                       @"almoco"];
        
        MainMealModel *almoco = [[MainMealModel alloc]init];
        
        almoco.acompanhamento = [tagAlmoco valueWithPath:@"acompanhamento"];
        almoco.guarnicao = [tagAlmoco valueWithPath:@"guarnicao"];
        almoco.principal = [tagAlmoco valueWithPath:@"principal"];
        almoco.salada = [tagAlmoco valueWithPath:@"salada"];
        almoco.sobremesa = [tagAlmoco valueWithPath:@"sobremesa"];
        almoco.suco = [tagAlmoco valueWithPath:@"suco"];
        almoco.vegetariano = [tagAlmoco valueWithPath:@"vegetariano"];
        
        [refeicoes addObject:almoco];
    }
    
    NSArray *tagJantars = [document.root children];
    
    for (int i = 0; i<[tagJantars count]; i++) {
        SMXMLElement *tagJantar = (SMXMLElement *)[tagJantars objectAtIndex:i];
        
        tagJantar = [tagJantar childNamed:
                     @"jantar"];
        MainMealModel *jantar = [[MainMealModel alloc]init];
        
        jantar.acompanhamento = [tagJantar valueWithPath:@"acompanhamento"];
        jantar.guarnicao = [tagJantar valueWithPath:@"guarnicao"];
        jantar.principal = [tagJantar valueWithPath:@"principal"];
        jantar.salada = [tagJantar valueWithPath:@"salada"];
        jantar.sobremesa = [tagJantar valueWithPath:@"sobremesa"];
        jantar.suco = [tagJantar valueWithPath:@"suco"];
        jantar.vegetariano = [tagJantar valueWithPath:@"vegetariano"];
        
        [refeicoes addObject:jantar];
    }
    
    return refeicoes;
}
- (BreakFastModel *)getBreakfastToday{
    if(refeicoes == Nil){
        
        refeicoes =   [self getRefeicoes];
        
    }
    RUAuxiliar *aux = [[RUAuxiliar alloc]init];
    NSString *weekDay = [aux getCurrentDay];
    BreakFastModel  *desjejum ;
    
    if([weekDay isEqualToString:@"segunda-feira"] || [weekDay isEqualToString:@"Monday"]){
        desjejum = [refeicoes objectAtIndex:0];
    }else if([weekDay isEqualToString:@"terça-feira"] || [weekDay isEqualToString:@"Tuesday"]){
        desjejum = [refeicoes objectAtIndex:1];
    }else if([weekDay isEqualToString:@"quarta-feira"] || [weekDay isEqualToString:@"Wednesday"]){
        desjejum = [refeicoes objectAtIndex:2];
    }else if([weekDay isEqualToString:@"quinta-feira"] || [weekDay isEqualToString:@"Thursday"]){
        desjejum = [refeicoes objectAtIndex:3];
    }else if([weekDay isEqualToString:@"sexta-feira"] || [weekDay isEqualToString:@"Friday"]){
        desjejum = [refeicoes objectAtIndex:4];
    }
    
    return desjejum;
}

- (MainMealModel *)getLunchToday{
    if(refeicoes == Nil){
        
        refeicoes =   [self getRefeicoes];
        
    }
    RUAuxiliar *aux = [[RUAuxiliar alloc]init];
    NSString *weekDay = [aux getCurrentDay];
    MainMealModel *almoco ;
    
    if([weekDay isEqualToString:@"segunda"] || [weekDay isEqualToString:@"Monday"]){
        almoco = [refeicoes objectAtIndex:5];
    }else if([weekDay isEqualToString:@"terça"] || [weekDay isEqualToString:@"Tuesday"]){
        almoco = [refeicoes objectAtIndex:6];
    }else if([weekDay isEqualToString:@"quarta"] || [weekDay isEqualToString:@"Wednesday"]){
        almoco = [refeicoes objectAtIndex:7];
    }else if([weekDay isEqualToString:@"quinta"] || [weekDay isEqualToString:@"Thursday"]){
        almoco = [refeicoes objectAtIndex:8];
    }else if([weekDay isEqualToString:@"sexta"] || [weekDay isEqualToString:@"Friday"]){
        almoco = [refeicoes objectAtIndex:9];
    }
    
    return almoco;
}


- (MainMealModel *)getDinnerToday{
    if(refeicoes == Nil){
        
        refeicoes =   [self getRefeicoes];
        
    }    RUAuxiliar *aux = [[RUAuxiliar alloc]init];
    NSString *weekDay = [aux getCurrentDay];
    MainMealModel *jantar ;
    
    if([weekDay isEqualToString:@"segunda"] || [weekDay isEqualToString:@"Monday"]){
        jantar = [refeicoes objectAtIndex:10];
    }else if([weekDay isEqualToString:@"terça"] || [weekDay isEqualToString:@"Tuesday"]){
        jantar = [refeicoes objectAtIndex:11];
    }else if([weekDay isEqualToString:@"quarta"] || [weekDay isEqualToString:@"Wednesday"]){
        jantar = [refeicoes objectAtIndex:12];
    }else if([weekDay isEqualToString:@"quinta"] || [weekDay isEqualToString:@"Thursday"]){
        jantar = [refeicoes objectAtIndex:13];
    }else if([weekDay isEqualToString:@"sexta"] || [weekDay isEqualToString:@"Friday"]){
        jantar = [refeicoes objectAtIndex:14];
    }
    
    return jantar;
}


@end
