//
//  Courses.h
//  iSA
//
//  Created by Nicolau Brasil on 02/06/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Courses : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UITextFieldDelegate> {
}

@property (nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic) UIActivityIndicatorView *loadingSpinner;

@end
