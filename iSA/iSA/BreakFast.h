//
//  BreakFast.h
//  iSA
//
//  Created by Nicolau Brasil on 20/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BreakFastModel.h"

@interface BreakFast : UIViewController

@property (nonatomic) NSMutableArray *breakFastListSections;
@property (nonatomic) BreakFastModel *breakfastToday;
@property (weak, nonatomic) IBOutlet UILabel *bebidas;
@end
