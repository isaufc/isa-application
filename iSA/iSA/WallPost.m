//
//  WallPost.m
//  iSA
//
//  Created by Afonso Neto on 20/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "WallPost.h"
#import "Parse/Parse.h"
#import "BackButton.h"

@interface WallPost ()

@end

@implementation WallPost

@synthesize imgToUpload;
@synthesize nickname;
@synthesize commentField;
@synthesize progressBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.title = @"Nova Postagem";
    UIButton *btSendPost =[[UIButton alloc] init];
    [btSendPost setBackgroundImage:[UIImage imageNamed:@"btSend.png"] forState:UIControlStateNormal];
    
    btSendPost.frame = CGRectMake(0, 0, 75, 30);
    UIBarButtonItem *btSend =[[UIBarButtonItem alloc] initWithCustomView:btSendPost];
    [btSendPost addTarget:self action:@selector(postInWall) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = btSend;
    commentField.delegate = self;
    UIImageView *myIcon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_commentField.png"]];
    [commentField setLeftView:myIcon];
    [commentField setLeftViewMode:UITextFieldViewModeAlways];
    [self.progressBar setProgress:0];
    self.progressBar.alpha = 0;
    [super viewDidLoad];
}

- (IBAction)selectPictureTouched:(id)sender {
    //Open a UIImagePickerController to select the picture
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    imgPicker.delegate = self;
    imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self.navigationController presentViewController:imgPicker animated:YES completion:nil];
}

- (void)postInWall {
    [self.commentField resignFirstResponder];
    
    //Validation to check if post description is not null or not empty
    if ([self.commentField.text isEqualToString:@""] || [self.commentField.text isEqual:nil]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"A descrição da imagem não pode ser vazia!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }

    //Disable the send button until we are ready
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    //Place the loading spinner
    UIActivityIndicatorView *loadingSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [loadingSpinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)];
    [loadingSpinner startAnimating];
    [self.view addSubview:loadingSpinner];
    
    //TODO: Upload a new picture
    NSData *pictureData = UIImagePNGRepresentation(self.imgToUpload.image);
    
    PFFile *imagem = [PFFile fileWithName:@"img" data:pictureData];
    [imagem saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            PFObject *encapsulateObj = [PFObject objectWithClassName:@"WallPost"];
            [encapsulateObj setObject:imagem forKey:@"Image"];
            [encapsulateObj setObject:[PFUser currentUser].username forKey:@"user"];
            [encapsulateObj setObject:[[PFUser currentUser] objectForKey:@"name"] forKey:@"userName"];
            [encapsulateObj setObject:[[PFUser currentUser] objectForKey:@"course"] forKey:@"course"];
            [encapsulateObj setObject:self.commentField.text forKey:@"comment"];
            [encapsulateObj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    [self.navigationController popViewControllerAnimated:YES];
                } else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível acessar o banco de dados, tente novamente mais tarde!." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível enviar a imagem selecionada!." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    } progressBlock:^(int percentDone) {
        float progress = percentDone / 100.0;
        self.progressBar.alpha = 1;
        [self.progressBar setProgress:progress];
    }];
}

#pragma mark UIImagePicker delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)img editingInfo:(NSDictionary *)editInfo
{
    self.imgToUpload.image = img;
    self.imgToUpload.contentMode = UIViewContentModeScaleAspectFit;
    
    //Validation to check if image selected is not nill
    if (self.imgToUpload.image == NULL) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Verifique a imagem selecionada!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }

    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.commentField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self setUpImageBackButton];
}

@end
