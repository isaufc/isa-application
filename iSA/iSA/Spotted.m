//
//  Spotted.m
//  iSA
//
//  Created by Afonso Neto on 13/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "Spotted.h"
#import "About.h"

@interface Spotted ()

@end

@implementation Spotted

@synthesize webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Spotted UFC";
    //Abrir App do Facebook na página do Spotted.
//    NSURL *url = [NSURL URLWithString:@"fb://profile/125602350964322"];
//    [[UIApplication sharedApplication] openURL:url];
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    NSString *urlAddress = @"https://m.facebook.com/spottedceara";
    NSURL *url = [[NSURL alloc] initWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:requestObj];
    self.webView.delegate = self;
    
    [self.view addSubview:self.webView];
}

//Method to roll the Spotted page to show only posts
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.webView stringByEvaluatingJavaScriptFromString:@"window.scrollBy(0, 650);"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
