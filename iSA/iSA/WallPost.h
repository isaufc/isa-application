//
//  WallPost.h
//  iSA
//
//  Created by Afonso Neto on 20/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WallPost : UIViewController <UIPickerViewDelegate, UITextFieldDelegate>

@property (nonatomic) IBOutlet UIImageView *imgToUpload;
@property (nonatomic) IBOutlet UITextField *commentField;
@property (nonatomic) IBOutlet UIProgressView *progressBar;
@property (nonatomic) NSString *nickname;

-(IBAction)selectPictureTouched:(id)sender;

@end
