//
//  Splash.m
//  iSA
//
//  Created by Afonso Neto on 18/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "Splash.h"
#import "Menu.h"

@interface Splash ()

@end

@implementation Splash

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewDidAppear:(BOOL)animated {
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"splash2" withExtension:@"mp4"];
    MPMoviePlayerViewController *splash = [[MPMoviePlayerViewController alloc] initWithContentURL:url];
    [self presentMoviePlayerViewControllerAnimated:splash];
    [splash.moviePlayer setControlStyle:MPMovieControlStyleNone];
    [[splash view] setFrame:[[UIScreen mainScreen] bounds]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(splashEnd:) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    
    //During 1.6 seconds, the alpha of the splash video is animated from 0 to 1 (what makes a fade in effect) to avoid the problem with the presentMoviePlayerViewControllerAnimated method effect.
    splash.view.alpha = 0.0;
    [UIView animateWithDuration:1.6 animations:^{
        splash.view.alpha = 1.0;
    }];
    
    [splash.moviePlayer play];
    [self.view addSubview:[splash view]];
}

//Method executed when splash exhibition ends.
-(IBAction)splashEnd:(id)sender {
    Menu *menuView = [[Menu alloc] init];
    [self.navigationController pushViewController:menuView animated:YES];
    [self.navigationController.navigationBar setHidden:NO];
}

//Method called before the display of the splash to hide the navigation bar on it.
- (void) viewWillAppear:(BOOL)animated {
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
