//
//  Desjejum.h
//  iSA
//
//  Created by Daniel Carlos on 20/10/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BreakFastModel : NSObject{
}
@property (nonatomic,retain)NSString *bebidas;
@property (nonatomic,retain)NSString *especial;
@property (nonatomic,retain)NSString *frutas;
@property (nonatomic,retain)NSString *paes;
@end
