//
//  Chat.h
//  iSA
//
//  Created by Afonso Neto on 15/06/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Chat : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UITextFieldDelegate> {}

@property (nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) IBOutlet UITextField *messageInput;
@property (nonatomic) NSTimer *timer;
@property (nonatomic) UIActivityIndicatorView *loadingSpinner;

-(IBAction)sendMessage:(id)sender;
-(void)scheduledLoad;
-(void)automaticScroll;
-(NSDate *)getCurrentDayAt00;
-(void)showSpinner;

@end
