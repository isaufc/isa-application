//
//  ChatTableCell.h
//  iSA
//
//  Created by Afonso Neto on 16/06/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatTableCell : UITableViewCell {}

@property (nonatomic) IBOutlet UITextView *message;
@property (nonatomic) IBOutlet UILabel *username;
@property (nonatomic) IBOutlet UILabel *courseName;
@property (nonatomic) IBOutlet UILabel *timeSent;

@end
