//
//  RUAuxiliar.h
//  iSA
//
//  Created by Afonso Neto on 02/06/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RUAuxiliar : UIViewController

- (NSString *)getCurrentDay;

@end
