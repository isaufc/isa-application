//
//  Dinner.h
//  iSA
//
//  Created by Nicolau Brasil on 27/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainMealModel.h"


@interface Dinner : UIViewController

@property(nonatomic) MainMealModel *dinnerToday;

@end
