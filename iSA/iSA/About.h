//
//  About.h
//  iSA
//
//  Created by Afonso Neto on 13/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface About : UIViewController

- (IBAction)btClose:(id)sender;

@end
