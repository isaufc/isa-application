//
//  MapList.h
//  iSA
//
//  Created by Afonso Neto on 09/06/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapList : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UITextFieldDelegate> {
}

@property (nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic) UIActivityIndicatorView *loadingSpinner;

-(void)showSpinner;

@end
