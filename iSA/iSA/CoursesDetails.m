//
//  CoursesDetails.m
//  iSA
//
//  Created by Nicolau Brasil on 08/06/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "CoursesDetails.h"
#import <Parse/Parse.h>
#import "BackButton.h"

@interface CoursesDetails () {
    NSMutableArray *allDesc;
}

@end

@implementation CoursesDetails

@synthesize ctitle, courseFromlistView, description, image, loadingSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    allDesc = [[NSMutableArray alloc] init];
    //self.title = self.courseFromlistView;
    self.title = @"Curso";
    self.ctitle.text = self.courseFromlistView;
    [self showSpinner];
    PFQuery *query = [[PFQuery alloc] initWithClassName:@"Courses"];
    [query orderByAscending:@"title"];
    [query whereKey:@"title" equalTo:self.courseFromlistView];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            [self.loadingSpinner removeFromSuperview];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível carregar verifique sua conexão e tente novamente!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        } else {
            [self.loadingSpinner removeFromSuperview];
            NSMutableArray *descriptions = [[NSMutableArray alloc] initWithArray:objects];
            for (PFObject *parseObject in descriptions) {
                NSString *descrFromBD = [[NSString alloc] initWithString:[parseObject objectForKey:@"description"]];
                [allDesc addObject:descrFromBD];
                description.font = [UIFont fontWithName:@"Arial" size:12];
                description.textColor = [UIColor whiteColor];
                description.textAlignment = UITextAlignmentLeft; //VER OHATTRIBUTEDLABEL
                description.text = [allDesc objectAtIndex:0];
                PFFile *images = (PFFile *)[parseObject objectForKey:@"image"];
                self.image = [[UIImageView alloc] initWithImage:[UIImage imageWithData:[images getData]]];
                image.frame = CGRectMake(10, 60, 300, 136);
                [self.view addSubview:image];
            }
        }
    }];
}

-(IBAction)showCurriculum:(id)sender {
    UIViewController *webViewController = [[UIViewController alloc] init];
    NSString *path = [[NSBundle mainBundle] pathForResource:self.courseFromlistView ofType:@"pdf"];
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    UIWebView *courseCurriculum = [[UIWebView alloc] initWithFrame:CGRectMake(0,0,320,568)];
    courseCurriculum.scalesPageToFit = YES; // Zoom
    webViewController.title = @"Matriz Curricular";
    [courseCurriculum loadRequest:request];
    [webViewController.view addSubview:courseCurriculum];
    [self setUpImageBackButton:webViewController];
    [self.navigationController pushViewController:webViewController animated:YES];
}

-(void)showSpinner {
    self.loadingSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.loadingSpinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)];
    [self.loadingSpinner startAnimating];
    [self.view addSubview:self.loadingSpinner];
}

- (void)setUpImageBackButton:(UIViewController *)webView {
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 22)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"backButton.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barBackButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton addTarget:self action:@selector(popCurrentViewController) forControlEvents:UIControlEventTouchUpInside];
    backButton.adjustsImageWhenHighlighted = NO;
    webView.navigationItem.leftBarButtonItem = barBackButtonItem;
    webView.navigationItem.hidesBackButton = YES;
}

- (void)popCurrentViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self setUpImageBackButton];
}


@end
