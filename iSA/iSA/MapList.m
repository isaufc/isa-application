//
//  MapList.m
//  iSA
//
//  Created by Afonso Neto on 09/06/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "MapList.h"
#import "Map.h"
#import "BathroomPlaces.h"
#import "WaterDrinkerPlaces.h"
#import "XeroxPlaces.h"
#import "CanteenPlaces.h"
#import "Parse/Parse.h"
#import "BackButton.h"

@interface MapList () {
    NSMutableArray *totalPlaces;
    NSMutableArray *placeList;
    NSMutableArray *filteredCourses;
    BOOL isFiltered;
}

@end

@implementation MapList

@synthesize searchBar, tableView, loadingSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.title = @"Locais";
    self.searchDisplayController.searchResultsTableView.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(45/255.0) blue:(105/255.0) alpha:1];
    [super viewDidLoad];
    totalPlaces = [[NSMutableArray alloc] init];
    self.searchBar.delegate = self;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self showSpinner];
    PFQuery *query = [[PFQuery alloc] initWithClassName:@"Place"];
    [query orderByAscending:@"title"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            [self.loadingSpinner removeFromSuperview];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível carregar a lista de cursos, verifique sua conexão e tente novamente!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        } else {
            [self.loadingSpinner removeFromSuperview];
            placeList = [[NSMutableArray alloc] initWithArray:objects];
            [tableView reloadData];
            for (PFObject *parseObject in placeList) {
                NSString *titleFromBD = [[NSString alloc] initWithString:[parseObject objectForKey:@"title"]];
                [totalPlaces addObject:titleFromBD];
            }
            [self.tableView reloadData];
        }
    }];
    UIButton *btSendPost =[[UIButton alloc] init];
    [btSendPost setBackgroundImage:[UIImage imageNamed:@"btServicesPlaces.png"] forState:UIControlStateNormal];
    
    btSendPost.frame = CGRectMake(0, 0, 90, 30);
    UIBarButtonItem *btSend =[[UIBarButtonItem alloc] initWithCustomView:btSendPost];
    [btSendPost addTarget:self action:@selector(gotoFilterPlaces) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = btSend;
    // Do any additional setup after loading the view from its nib.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Map *mapView = [[Map alloc] init];
    if (!isFiltered) {
        NSString *placeTitle = [totalPlaces objectAtIndex:indexPath.row];
        mapView.placeTitle = placeTitle;
        [self.navigationController pushViewController:mapView animated:YES];
    } else {
        NSString *placeTitle = [filteredCourses objectAtIndex:indexPath.row];
        mapView.placeTitle = placeTitle;
        [self.navigationController pushViewController:mapView animated:YES];
    }
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length == 0) {
        isFiltered = NO;
    } else {
        isFiltered = YES;
        filteredCourses = [[NSMutableArray alloc] init];
        for (NSString *str in totalPlaces) {
            NSRange stringRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (stringRange.location != NSNotFound) {
                [filteredCourses addObject:str];
            }
        }
    }
    [self.tableView reloadData];
}

-(void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar {
    [self.tableView resignFirstResponder];
}

// Return number of courses.
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isFiltered) {
        return [filteredCourses count];
    }
    return [totalPlaces count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Create cell
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        //Faz o cache da célula para evitar criar muitos objetos desnecessários durante o scroll
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (!isFiltered) {
        cell.textLabel.text = [totalPlaces objectAtIndex:indexPath.row];
    } else {
        cell.textLabel.text = [filteredCourses objectAtIndex:indexPath.row];
    }
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.accessoryView.backgroundColor = [UIColor redColor];
    tableView.separatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"separator"]];
    return cell;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.searchBar resignFirstResponder];
}

- (void)gotoFilterPlaces {
    UITabBarController *searchPlacesView = [[UITabBarController alloc] init];
    // Change tabBar Color
    UIImage *tabBackground = [[UIImage imageNamed:@"tabbar"]
                              resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [[UITabBar appearance] setBackgroundImage:tabBackground];
    [[UITabBar appearance] setSelectionIndicatorImage:
     [UIImage imageNamed:@"tab_select_indicator"]];
    [UITabBarItem.appearance setTitleTextAttributes:@{
                          UITextAttributeTextColor : [UIColor grayColor] } forState:UIControlStateNormal];
    [UITabBarItem.appearance setTitleTextAttributes:@{
                          UITextAttributeTextColor : [UIColor colorWithRed:(0/255.0) green:(121/255.0) blue:(255/255.0) alpha:1] }     forState:UIControlStateSelected];
    // Tabs
    BathroomPlaces *bathList = [[BathroomPlaces alloc] init];
    bathList.title = @"Banheiros";
    XeroxPlaces *xList = [[XeroxPlaces alloc] init];
    xList.title = @"Xerox";
    CanteenPlaces *cPlaces = [[CanteenPlaces alloc] init];
    cPlaces.title = @"Cantinas";
    WaterDrinkerPlaces *wDPlaces = [[WaterDrinkerPlaces alloc] init];
    wDPlaces.title = @"Bebedouros";
    // Icons
    UIImage *selectedimgBath = [UIImage imageNamed:@"bath-se.png"];
    UIImage *unselectedimgBath = [UIImage imageNamed:@"bath.png"];
    [bathList.tabBarItem setFinishedSelectedImage:selectedimgBath withFinishedUnselectedImage:unselectedimgBath];
    UIImage *selectedimgXerox = [UIImage imageNamed:@"xerox-se.png"];
    UIImage *unselectedimgXerox = [UIImage imageNamed:@"xerox.png"];
    [xList.tabBarItem setFinishedSelectedImage:selectedimgXerox withFinishedUnselectedImage:unselectedimgXerox];
    UIImage *selectedimgCanteen = [UIImage imageNamed:@"canteen-se.png"];
    UIImage *unselectedimgCanteen = [UIImage imageNamed:@"canteen.png"];
    [cPlaces.tabBarItem setFinishedSelectedImage:selectedimgCanteen withFinishedUnselectedImage:unselectedimgCanteen];
    UIImage *selectedimgWater = [UIImage imageNamed:@"waterDrink-se.png"];
    UIImage *unselectedimgWater = [UIImage imageNamed:@"waterDrink.png"];
    [wDPlaces.tabBarItem setFinishedSelectedImage:selectedimgWater withFinishedUnselectedImage:unselectedimgWater];
    searchPlacesView.navigationItem.title = @"Serviços - PICI";
    searchPlacesView.viewControllers = [NSArray arrayWithObjects:bathList, xList, cPlaces, wDPlaces, nil];
    [self setUpImageBackButton:searchPlacesView];
    [self.navigationController pushViewController:searchPlacesView animated:YES];
}

-(void)showSpinner {
    self.loadingSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.loadingSpinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)];
    [self.loadingSpinner startAnimating];
    [self.view addSubview:self.loadingSpinner];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self setUpImageBackButton];
}

- (void)setUpImageBackButton:(UITabBarController *)tabBar {
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 22)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"backButton.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barBackButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton addTarget:self action:@selector(popCurrentViewController) forControlEvents:UIControlEventTouchUpInside];
    backButton.adjustsImageWhenHighlighted = NO;
    tabBar.navigationItem.leftBarButtonItem = barBackButtonItem;
    tabBar.navigationItem.hidesBackButton = YES;
}

- (void)popCurrentViewController {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
