//
//  XeroxPlaces.m
//  iSA
//
//  Created by Afonso Neto on 09/06/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "XeroxPlaces.h"
#import "Map.h"
#import "Parse/Parse.h"

@interface XeroxPlaces () {
    NSMutableArray *totalPlaces;
    NSMutableArray *placeList;
    NSMutableArray *filteredCourses;
    BOOL isFiltered;
}

@end

@implementation XeroxPlaces

@synthesize searchBar, tableView, loadingSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.searchDisplayController.searchResultsTableView.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(45/255.0) blue:(105/255.0) alpha:1];
    // Do any additional setup after loading the view from its nib.
    totalPlaces = [[NSMutableArray alloc] init];
    [totalPlaces addObject:@"Todos os lugares com Xerox"];
    self.searchBar.delegate = self;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self showSpinner];
    PFQuery *query = [[PFQuery alloc] initWithClassName:@"Place"];
    [query whereKey:@"hasXerox" equalTo:[NSNumber numberWithBool:TRUE]];
    [query orderByAscending:@"title"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            [self.loadingSpinner removeFromSuperview];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível carregar a lista de lugares, verifique sua conexão e tente novamente!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        } else {
            [self.loadingSpinner removeFromSuperview];
            placeList = [[NSMutableArray alloc] initWithArray:objects];
            [tableView reloadData];
            for (PFObject *parseObject in placeList) {
                NSString *titleFromBD = [[NSString alloc] initWithString:[parseObject objectForKey:@"title"]];
                [totalPlaces addObject:titleFromBD];
            }
            [self.tableView reloadData];
        }
    }];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Map *mapView = [[Map alloc] init];
    if (!isFiltered) {
        NSString *placeTitle = [totalPlaces objectAtIndex:indexPath.row];
        mapView.placeTitle = placeTitle;
        [self.navigationController pushViewController:mapView animated:YES];
    } else {
        NSString *placeTitle = [filteredCourses objectAtIndex:indexPath.row];
        mapView.placeTitle = placeTitle;
        [self.navigationController pushViewController:mapView animated:YES];
    }
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length == 0) {
        isFiltered = NO;
    } else {
        isFiltered = YES;
        filteredCourses = [[NSMutableArray alloc] init];
        for (NSString *str in totalPlaces) {
            NSRange stringRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (stringRange.location != NSNotFound) {
                [filteredCourses addObject:str];
            }
        }
    }
    [self.tableView reloadData];
}

-(void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar {
    [self.tableView resignFirstResponder];
}

// Return number of courses.
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isFiltered) {
        return [filteredCourses count];
    }
    return [totalPlaces count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Create cell
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        //Faz o cache da célula para evitar criar muitos objetos desnecessários durante o scroll
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (!isFiltered) {
        cell.textLabel.text = [totalPlaces objectAtIndex:indexPath.row];
    } else {
        cell.textLabel.text = [filteredCourses objectAtIndex:indexPath.row];
    }
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.accessoryView.backgroundColor = [UIColor redColor];
    tableView.separatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"separator"]];
    return cell;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.searchBar resignFirstResponder];
}

-(void)showSpinner {
    self.loadingSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.loadingSpinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)];
    [self.loadingSpinner startAnimating];
    [self.view addSubview:self.loadingSpinner];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
