//
//  Menu.m
//  iSA
//
//  Created by Afonso Neto on 13/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "Menu.h"
#import "Parse/Parse.h"
#import "Signin.h"
#import "Chat.h"
#import "Wall.h"
#import "About.h"
#import "Spotted.h"
#import "Lunch.h"
#import "BreakFast.h"
#import "Dinner.h"
#import "Courses.h"
#import "Telephone.h"
#import "Bus.h"
#import "Map.h"
#import "RUService.h"
#import "BreakFastModel.h"
#import "MainMealModel.h"

@interface Menu ()

@end

@implementation Menu

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Title for the menu view
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"top_logo.png"]];
    self.view.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(45/255.0) blue:(105/255.0) alpha:1];
    //Button "Créditos" in NavigationBar
    UIButton* aboutButton = [UIButton buttonWithType:UIButtonTypeInfoDark];
    [aboutButton addTarget:self action:@selector(gotoAbout:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:aboutButton];
    self.navigationItem.hidesBackButton = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

//Code executed when user clicks in the Chat button
-(IBAction)gotoChat:(id)sender {
    //Test if there's some user logged in
    if ([PFUser currentUser].isAuthenticated) {
        //If true, it's redirected to Chat
        Chat *chatView = [[Chat alloc] init];
        [self.navigationController pushViewController:chatView animated:YES];
    } else {
        //If it's false, redirect him to Sign in view
        Signin *signinView = [[Signin alloc] init];
        signinView.redirectTo = @"Chat";
        [self.navigationController pushViewController:signinView animated:true];
    }
}

//Code executed when user clicks in the "Mural" button
-(IBAction)gotoWall:(id)sender {
    //Test if there's some user logged in
    if ([PFUser currentUser].isAuthenticated) {
        //If true, it's redirected to the Wall
        Wall *wallView = [[Wall alloc] init];
        [self.navigationController pushViewController:wallView animated:true];
    } else {
        //If it's false, redirect him to Sign in view
        Signin *signinView = [[Signin alloc] init];
        signinView.redirectTo = @"Wall";
        [self.navigationController pushViewController:signinView animated:true];
    }
}

//Code executed when user clicks in the Spotted button
-(IBAction)gotoSpotted:(id)sender {
    Spotted *spottedView = [[Spotted alloc] init];
    [self.navigationController pushViewController:spottedView animated:YES];
}

//Code executed when user clicks in the "RU" button
-(IBAction)gotoRU:(id)sender {
    
    
    // Change tabBar Color
    UIImage *tabBackground = [[UIImage imageNamed:@"tabbar"]
                              resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [[UITabBar appearance] setBackgroundImage:tabBackground];
    [[UITabBar appearance] setSelectionIndicatorImage:
    [UIImage imageNamed:@"tab_select_indicator"]];
    [UITabBarItem.appearance setTitleTextAttributes:@{
                                                      UITextAttributeTextColor : [UIColor grayColor] } forState:UIControlStateNormal];
    [UITabBarItem.appearance setTitleTextAttributes:@{
                                                      UITextAttributeTextColor : [UIColor colorWithRed:(0/255.0) green:(121/255.0) blue:(255/255.0) alpha:1] }     forState:UIControlStateSelected];
    
    RUService *service = [[RUService alloc]init];
    BreakFastModel *brekfast = [service getBreakfastToday];
    MainMealModel *lunch = [service getLunchToday];
    MainMealModel *dinner = [service getDinnerToday];
    
    // Tabs
    
    //BreakFast Tab
    BreakFast *breakfastNav = [[BreakFast alloc] init];
    breakfastNav.breakfastToday = brekfast;
    breakfastNav.tabBarItem.title = @"Desjejum";
    
    UIImage *selectedimgBFast = [UIImage imageNamed:@"breakfast-se.png"];
    UIImage *unselectedimgBFast = [UIImage imageNamed:@"breakfast.png"];
    [breakfastNav.tabBarItem setFinishedSelectedImage:selectedimgBFast withFinishedUnselectedImage:unselectedimgBFast];
    
    
    //Lunch Tab
    Lunch *lunchNav = [[Lunch alloc] init];
    lunchNav.lunchToday = lunch;
    lunchNav.tabBarItem.title = @"Almoço";
    
    
    UIImage *selectedimgLunch = [UIImage imageNamed:@"lunch-se.png"];
    UIImage *unselectedimgLunch = [UIImage imageNamed:@"lunch.png"];
    [lunchNav.tabBarItem setFinishedSelectedImage:selectedimgLunch withFinishedUnselectedImage:unselectedimgLunch];
    
    //Dinner Tab
    Dinner *dinnerNav = [[Dinner alloc] init];
    dinnerNav.dinnerToday = dinner;
    dinnerNav.tabBarItem.title = @"Jantar";
    
    UIImage *selectedimgDinner = [UIImage imageNamed:@"dinner-se.png"];
    UIImage *unselectedimgDinner = [UIImage imageNamed:@"dinner.png"];
    [dinnerNav.tabBarItem setFinishedSelectedImage:selectedimgDinner withFinishedUnselectedImage:unselectedimgDinner];
    
    
    UITabBarController *ruView = [[UITabBarController alloc] init];
    ruView.navigationItem.title = @"Restaurante Universitário";
    ruView.viewControllers = [NSArray arrayWithObjects:breakfastNav, lunchNav, dinnerNav, nil];
    [self.navigationController pushViewController:ruView animated:YES];
    [self setUpImageBackButton:ruView];
}

//Code executed when user click in the "Universidade" button
-(IBAction)gotoUniversity:(id)sender {
    UITabBarController *universityView = [[UITabBarController alloc] init];
    // Change tabBar Color
    UIImage *tabBackground = [[UIImage imageNamed:@"tabbar"]
                              resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [[UITabBar appearance] setBackgroundImage:tabBackground];
    [[UITabBar appearance] setSelectionIndicatorImage:
     [UIImage imageNamed:@"tab_select_indicator"]];
    [[UITabBar appearance] setTintColor:[UIColor grayColor]];
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:(0/255.0) green:(121/255.0) blue:(255/255.0) alpha:0.5]];
    [UITabBarItem.appearance setTitleTextAttributes:@{
                                                      UITextAttributeTextColor : [UIColor grayColor] } forState:UIControlStateNormal];
    [UITabBarItem.appearance setTitleTextAttributes:@{
                                                      UITextAttributeTextColor : [UIColor colorWithRed:(0/255.0) green:(121/255.0) blue:(255/255.0) alpha:1] }     forState:UIControlStateSelected];
    // Tabs
    Courses *nav4 = [[Courses alloc] init];
    Telephone *nav5 = [[Telephone alloc] init];
    nav4.tabBarItem.title = @"Cursos";
    nav5.tabBarItem.title = @"Telefones Úteis";
    // Courses images
    UIImage *selectedimgCourses = [UIImage imageNamed:@"courses.png"];
    UIImage *unselectedimgCourses = [UIImage imageNamed:@"courses-un.png"];
    [nav4.tabBarItem setFinishedSelectedImage:selectedimgCourses withFinishedUnselectedImage:unselectedimgCourses];
    // Telephone images
    UIImage *selectedimgTel = [UIImage imageNamed:@"telephone-se.png"];
    UIImage *unselectedimgTel = [UIImage imageNamed:@"telephone.png"];
    [nav5.tabBarItem setFinishedSelectedImage:selectedimgTel withFinishedUnselectedImage:unselectedimgTel];
    universityView.navigationItem.title = @"UFC";
    universityView.viewControllers = [NSArray arrayWithObjects:nav4, nav5, nil];
    [self.navigationController pushViewController:universityView animated:YES];
    [self setUpImageBackButton:universityView];
    
}

//Code executed when user click in the "Locais" button
-(IBAction)gotoPlaces:(id)sender {
    /*UITabBarController *placesView = [[UITabBarController alloc] init];
     // Change tabBar Color
     UIImage *tabBackground = [[UIImage imageNamed:@"tabbar"]
     resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
     [[UITabBar appearance] setBackgroundImage:tabBackground];
     [[UITabBar appearance] setSelectionIndicatorImage:
     [UIImage imageNamed:@"tab_select_indicator"]];
     [[UITabBar appearance] setTintColor:[UIColor clearColor]];
     [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:(255/255.0) green:(140/255.0) blue:(0/255.0) alpha:1]];
     [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
     [UIColor whiteColor], UITextAttributeTextColor,
     [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)], UITextAttributeTextShadowOffset,
     nil] forState:UIControlStateNormal];
     
     // Tabs
     Bus *busView = [[Bus alloc] init];
     Map *mapsView = [[Map alloc] init];
     busView.tabBarItem.title = @"Ônibus";
     mapsView.tabBarItem.title = @"Mapa";
     busView.tabBarItem.image = [UIImage imageNamed:@"courses.png"];
     mapsView.tabBarItem.image = [UIImage imageNamed:@"telephone.png"];
     placesView.navigationItem.title = @"Lugares";
     placesView.viewControllers = [NSArray arrayWithObjects:mapsView, busView, nil];
     [self.navigationController pushViewController:placesView animated:YES];
     [self setUpImageBackButton:placesView];*/
    Map *mapView = [[Map alloc] init];
    [self.navigationController pushViewController:mapView animated:true];
}

- (void)setUpImageBackButton:(UITabBarController *)tabBar {
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 22)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"backButton.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barBackButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton addTarget:self action:@selector(popCurrentViewController) forControlEvents:UIControlEventTouchUpInside];
    backButton.adjustsImageWhenHighlighted = NO;
    tabBar.navigationItem.leftBarButtonItem = barBackButtonItem;
    tabBar.navigationItem.hidesBackButton = YES;
}

- (void)popCurrentViewController {
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

//Code executed when user clicks in the "Info" button
-(IBAction)gotoAbout:(id)sender {
    About *aboutView = [[About alloc] init];
    [self.navigationController pushViewController:aboutView animated:true];
}

@end
