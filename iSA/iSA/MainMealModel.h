//
//  RefeicaoPrincipal.h
//  iSA
//
//  Created by Daniel Carlos on 20/10/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainMealModel : NSObject{
}
@property (nonatomic,retain) NSString *acompanhamento;
@property (nonatomic,retain) NSString *guarnicao;
@property (nonatomic,retain) NSString *principal;
@property (nonatomic,retain) NSString *salada;
@property (nonatomic,retain) NSString *sobremesa;
@property (nonatomic,retain) NSString *suco;
@property (nonatomic,retain) NSString *vegetariano;

@end
