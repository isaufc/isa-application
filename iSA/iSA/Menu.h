//
//  Menu.h
//  iSA
//
//  Created by Afonso Neto on 13/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface Menu : UIViewController {}

-(IBAction)gotoChat:(id)sender;
-(IBAction)gotoWall:(id)sender;
-(IBAction)gotoSpotted:(id)sender;
-(IBAction)gotoRU:(id)sender;
-(IBAction)gotoUniversity:(id)sender;
-(IBAction)gotoPlaces:(id)sender;
-(IBAction)gotoAbout:(id)sender;

@end
