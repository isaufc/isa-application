//
//  HttpHelper.m
//  Carros
//
//  Created by Daniel Carlos on 14/08/13.
//  Copyright (c) 2013 Daniel Carlos Souza Carvalho. All rights reserved.
//

#import "HttpHelper.h"

@implementation HttpHelper
-(NSData *)doGet:(NSString *)url{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]cachePolicy:(NSURLRequestReloadIgnoringCacheData) timeoutInterval:30];
    NSHTTPURLResponse *urlResponse = nil;
    NSError *erro = nil;
    //requisição síncrona
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&erro];
    return data;
}
@end
