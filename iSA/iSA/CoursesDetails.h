//
//  CoursesDetails.h
//  iSA
//
//  Created by Nicolau Brasil on 08/06/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoursesDetails : UIViewController

@property (nonatomic) IBOutlet UILabel *ctitle;
@property (nonatomic) NSString *courseFromlistView;
@property (nonatomic) IBOutlet UILabel *description;
@property (nonatomic) IBOutlet UIImageView *image;
@property (nonatomic) UIActivityIndicatorView *loadingSpinner;

-(IBAction)showCurriculum:(id)sender;

@end
