//
//  About.m
//  iSA
//
//  Created by Afonso Neto on 13/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "About.h"
#import "Menu.h"
#import "BackButton.h"

@interface About ()

@end

@implementation About


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self setUpImageBackButton];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Sobre";
}

-(IBAction)btClose:(id)sender {
    Menu *menuView = [[Menu alloc] init];
    [self.navigationController pushViewController:menuView animated:true];
    [self.navigationController.navigationBar setHidden:NO];
}   

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
