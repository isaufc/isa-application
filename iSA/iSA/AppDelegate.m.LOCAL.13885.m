//
//  AppDelegate.m
//  iSA
//
//  Created by Afonso Neto on 13/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
//#import "Splash.h"
#import "Menu.h"

@implementation AppDelegate


- (void)customizeApp {
        // Set the background image for *all* UINavigationBars
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque animated:NO];
        UIImage *navBar = [UIImage imageNamed:@"navbar"];
        [[UINavigationBar appearance] setBackgroundImage:navBar forBarMetrics:UIBarMetricsDefault];
        // Set customize font for NavigationBar
        [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:                                                           [UIColor colorWithRed:0/255.0 green:45/255.0 blue:105/255.0 alpha:1.0], UITextAttributeTextColor,                                                        [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.8],UITextAttributeTextShadowColor,                                                           [NSValue valueWithUIOffset:UIOffsetMake(0, 1)],                                                           UITextAttributeTextShadowOffset,
            [UIFont fontWithName:@"HelveticaNeue-Bold" size:20.0], UITextAttributeFont, nil]];

}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Get customizeApp method.
    [self customizeApp];
    //Definition of this application to use Parse with the following AppID and ClientKey
    [Parse setApplicationId:@"qcKmX5QI2ufvr96Xvrxll3OE8UjAxT4AaiaNG5RR" clientKey:@"U89trk0lXYe76nXUVkDcBLxstyntueZILnxxquUJ"];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    //Instantiang the app menu, coloring it and making it as the root view
    Menu *menuView = [[Menu alloc] init];
    [menuView.view setBackgroundColor:[UIColor colorWithRed:(0/255.0) green:(45/255.0) blue:(105/255.0) alpha:1]];
    
    /* Splash *splashView = [[Splash alloc] init];
    [splashView.view setBackgroundColor:[UIColor colorWithRed:(0/255.0) green:(45/255.0) blue:(105/255.0) alpha:1]];*/
    
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:menuView];
    self.window.rootViewController = navigation;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    //Logout any user logged when the application goes for background
    //Maybe it's here only for development tests
    [PFUser logOut];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    
}

@end
