//
//  Signin.m
//  iSA
//
//  Created by Afonso Neto on 13/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "Signin.h"
#import "Parse/Parse.h"
#import "Wall.h"
#import "Chat.h"
#import "Signup.h"
#import "BackButton.h"

@interface Signin ()

@end

@implementation Signin

@synthesize redirectTo, loadingSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{    
    //Define the title to be showed in the Navigation Controller
    self.navigationItem.title = @"Acessar";
    
    //It tells to the iOS that this class will respond to handle the keyboard
    nickname.delegate = self;
    passwd.delegate = self;
    
    //Place the loading spinner
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self setUpImageBackButton];
}

//This method will be called when the user touches in any place of the application, and we're using to hide the keyboard when the user touches in a place of app that doesn't make any action
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [nickname resignFirstResponder];
    [passwd resignFirstResponder];
}

//This one is called when the user touches in the button "Return" of the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == nickname) {
        //If user click in return when the nickname field is selected, jump the cursor to passwd field
        [passwd becomeFirstResponder];
        return true;
    } else if (textField == passwd) {
        //If user click in return button when the passwd field is selected, call the login method
        [self loginTouched:nil];
        [nickname resignFirstResponder];
        [passwd resignFirstResponder];
        return true;
    }
    return false;
}

- (IBAction)loginTouched:(id)sender {
    if ([nickname.text isEqualToString:@""] || [passwd.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Você precisa preencher apelido e senha." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    } else {
        [self showSpinner];
        [PFUser logInWithUsernameInBackground:nickname.text password:passwd.text block:^(PFUser *user, NSError *error) {
            if (user) {
                if ([self.redirectTo isEqualToString:@"Chat"]) {
                    Chat *chatView = [[Chat alloc] init];
                    [self.navigationController pushViewController:chatView animated:true];
                } else if ([self.redirectTo isEqualToString:@"Wall"]) {
                    Wall *wallView = [[Wall alloc] init];
                    [self.navigationController pushViewController:wallView animated:true];
                }
            } else if (error) {
                [self.loadingSpinner removeFromSuperview];
                NSString *capturedError = [error localizedDescription];
                //Parse error 100: The Internet connection appears to be offline.
                NSString *connectionError = @"100";
                //Parse error 101: invalid login credentials.
                NSString *incorrectData = @"101";
                //Error nº 100 was found in the captured error string? If no, it's a invalid data problem
                if ([capturedError rangeOfString:connectionError].location == NSNotFound) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível realizar o login. Verifique os dados digitados." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                //Error nº 101 was found in the captured error string? If no, it's a connection problem
                } else if ([capturedError rangeOfString:incorrectData].location == NSNotFound) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível realizar o login. Verifique sua conexão com a internet e tente novamente!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                //Handling other error codes, only for safe! =)
                } else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Um problema desconhecido foi encontrado! Tente novamente!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }];
    }
}

-(IBAction)signupTouched:(id)sender {
    Signup *signupView = [[Signup alloc] init];
    signupView.redirectTo = self.redirectTo;
    [self.navigationController pushViewController:signupView animated:YES];
}

-(void)showSpinner {
    self.loadingSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.loadingSpinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)];
    [self.loadingSpinner startAnimating];
    [self.view addSubview:self.loadingSpinner];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
