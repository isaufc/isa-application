//
//  HttpHelper.h
//  Carros
//
//  Created by Daniel Carlos on 14/08/13.
//  Copyright (c) 2013 Daniel Carlos Souza Carvalho. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpHelper : NSObject{
}
-(NSData *) doGet:(NSString *)url;
@end
