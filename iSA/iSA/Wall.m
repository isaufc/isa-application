//
//  Wall.m
//  iSA
//
//  Created by Afonso Neto on 13/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "Wall.h"
#import "Menu.h"
#import "Parse/Parse.h"
#import "WallPost.h"


@interface Wall () {}

@property (nonatomic, retain) NSArray *wallObjectsArray;
@property (nonatomic, retain) UIActivityIndicatorView *activityIndicator;

-(void)getWallImages;
-(void)loadWallViews:(BOOL)dataRetrivied;

@end

@implementation Wall

@synthesize wallObjectsArray = _wallObjectsArray;
@synthesize wallScroll = _wallScroll;
@synthesize activityIndicator = _loadingSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self setUpImageBackButton];
    self.navigationItem.title = @"Mural";
    UIButton *btNewPost =[[UIButton alloc] init];
    [btNewPost setBackgroundImage:[UIImage imageNamed:@"bt_newPost.png"] forState:UIControlStateNormal];
    
    btNewPost.frame = CGRectMake(0, 0, 75, 30);
    UIBarButtonItem *btPost =[[UIBarButtonItem alloc] initWithCustomView:btNewPost];
    [btNewPost addTarget:self action:@selector(gotoUploadView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = btPost;
    [super viewDidLoad];
}

//Method called before view exibithion
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getWallImages];
}

//Method to recover the images from the server
- (void) getWallImages {
    
    UIActivityIndicatorView *loadingSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    [loadingSpinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)];
    [loadingSpinner startAnimating];
    
    [self.view addSubview:loadingSpinner];
    
    PFQuery *wallPosts = [PFQuery queryWithClassName:@"WallPost"];
    [wallPosts orderByDescending:@"createdAt"];
    [wallPosts findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (objects && [objects count] > 0) {
            self.wallObjectsArray = nil;
            self.wallObjectsArray = [[NSArray alloc] initWithArray:objects];
            [self loadWallViews:YES];
            [loadingSpinner removeFromSuperview];
        //If there's error, it's problably a missing internet connection
        } else if (error) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Ocorreu um erro ao tentar carregar o mural. Verifique sua conexão com a internet e tente novamente mais tarde." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [loadingSpinner removeFromSuperview];
        //Else, there's nothing in the Wall yet.
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Informação" message:@"Não há nenhuma postagem no mural." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [self loadWallViews:NO];
            [loadingSpinner removeFromSuperview];
        }
    }];
}

- (void) loadWallViews:(BOOL)dataRetrivied {
    //If came some data from the getWallImages method...
    if (dataRetrivied) {
        //Clear the actual wallScroll to add new content..
        for (id viewToRemove in [self.wallScroll subviews]) {
            if ([viewToRemove isMemberOfClass:[UIView class]])
                [viewToRemove removeFromSuperview];
        }
    }
    //For every wall element, put a view in the scroll
    int originY = 10;
    
    for (PFObject *wallObject in self.wallObjectsArray) {
        //Build the view with the image and the comments
        UIView *wallImageView = [[UIView alloc] initWithFrame:CGRectMake(10, originY, self.view.frame.size.width - 20 , 310)];
        wallImageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"board_bg.png"]];
        
        //Add the image
        PFFile *image = (PFFile *)[wallObject objectForKey:@"Image"];
        UIImageView *userImage = [[UIImageView alloc] initWithImage:[UIImage imageWithData:image.getData]];
        userImage.frame = CGRectMake(10, 23, wallImageView.frame.size.width - 20, 260);
        userImage.contentMode = UIViewContentModeScaleAspectFit;
        [userImage setBackgroundColor:[UIColor colorWithRed:(0/255.0) green:(45/255.0) blue:(105/255.0) alpha:1]];
        [wallImageView addSubview:userImage];

        //Add the info label (User and creation date)
        NSDate *creationDate = wallObject.createdAt;
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"HH:mm 'em' dd/MM/yyyy"];
        UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, wallImageView.frame.size.width,15)];
        infoLabel.text = [NSString stringWithFormat:@"Enviado por: %@ às %@", [wallObject objectForKey:@"userName"], [df stringFromDate:creationDate]];
        infoLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:10];
        infoLabel.textColor = [UIColor whiteColor];
        infoLabel.backgroundColor = [UIColor clearColor];
        [wallImageView addSubview:infoLabel];
        
        //Add the comment
        UILabel *commentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 288, wallImageView.frame.size.width, 15)];
        commentLabel.text = [wallObject objectForKey:@"comment"];
        commentLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
        commentLabel.textColor = [UIColor whiteColor];
        commentLabel.backgroundColor = [UIColor clearColor];
        [wallImageView addSubview:commentLabel];
        
        [self.wallScroll addSubview:wallImageView];
        
        originY = originY + wallImageView.frame.size.width + 20;
    }
    
    //7
    //Set the bounds of the scroll
    self.wallScroll.contentSize = CGSizeMake(self.wallScroll.frame.size.width, originY);
}

-(void)showErrorView:(NSString *)errorMsg{
    UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMsg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [errorAlertView show];
}

- (void) gotoUploadView {
    WallPost *wallPostView = [[WallPost alloc] init];
    [self.navigationController pushViewController:wallPostView animated:YES];
}

- (void)setUpImageBackButton {
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 22)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"backButton.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barBackButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton addTarget:self action:@selector(popCurrentViewController) forControlEvents:UIControlEventTouchUpInside];
    backButton.adjustsImageWhenHighlighted = NO;
    self.navigationItem.leftBarButtonItem = barBackButtonItem;
    self.navigationItem.hidesBackButton = YES;
}

- (void)popCurrentViewController {
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
