//
//  Chat.m
//  iSA
//
//  Created by Afonso Neto on 15/06/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "Chat.h"
#import "ChatTableCell.h"
#import "parse/Parse.h"

@interface Chat () {
    NSMutableArray *chatRows;
    NSMutableArray *tempChatRows;
    NSMutableArray *nicknames;
    NSMutableArray *courses;
    NSMutableArray *messages;
    NSMutableArray *messageTimes;
}

@end

@implementation Chat

@synthesize tableView, messageInput, timer, loadingSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)sendMessage:(id)sender {
    NSString *inputValue = self.messageInput.text;
    if (inputValue == nil || [inputValue isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não é possível enviar uma mensagem vazia!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    } else {
        PFUser *user = [PFUser currentUser];
        PFObject *newRow = [PFObject objectWithClassName:@"ChatRow"];
        [newRow setObject:[user objectForKey:@"name"] forKey:@"writer"];
        [nicknames addObject:[user objectForKey:@"name"]];
        [newRow setObject:[user objectForKey:@"course"] forKey:@"writerCourse"];
        [courses addObject:[user objectForKey:@"course"]];
        [newRow setObject:self.messageInput.text forKey:@"message"];
        [messages addObject:self.messageInput.text];
        NSDate *creationDate = [[NSDate alloc] init];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"'Hoje, às' HH:mm"];
        NSString *createdAt = [df stringFromDate:creationDate];
        [messageTimes addObject:createdAt];
        [newRow saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error && !succeeded) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível enviar sua mensagem. Verifique sua conexão e tente novamente!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            } else if (succeeded) {
                [chatRows addObject:newRow];
                [self.tableView reloadData];
                [self automaticScroll];
            }
        }];
    }
    self.messageInput.text = @"";
    [self.messageInput resignFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated {
    //Schedule the scheduledLoad method to run every 5 seconds
    timer = [NSTimer scheduledTimerWithTimeInterval:5.00 target:self selector:@selector(scheduledLoad) userInfo:nil repeats:YES];
    [self automaticScroll];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUpImageBackButton];
    self.navigationItem.title = @"Chat";
    self.messageInput.delegate = self;
    self.tableView.delegate = self;
    [self.tableView setAllowsSelection:NO];
    self.tableView.dataSource = self;
    
    nicknames = [[NSMutableArray alloc] init];
    courses = [[NSMutableArray alloc] init];
    messages = [[NSMutableArray alloc] init];
    messageTimes = [[NSMutableArray alloc] init];
    
    [self showSpinner];
    PFQuery *query = [[PFQuery alloc] initWithClassName:@"ChatRow"];
    [query orderByAscending:@"createdAt"];
    [query whereKey:@"createdAt" greaterThanOrEqualTo:[self getCurrentDayAt00]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            [self.loadingSpinner removeFromSuperview];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível carregar as mensagens agora, verifique sua conexão e tente novamente!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        } else {
            [self.loadingSpinner removeFromSuperview];
            chatRows = [[NSMutableArray alloc] initWithArray:objects];
            [tableView reloadData];
            for (PFObject *parseObject in chatRows) {
                NSString *username = [[NSString alloc] initWithString:[parseObject objectForKey:@"writer"]];
                NSString *userCourse = [[NSString alloc] initWithString:[parseObject objectForKey:@"writerCourse"]];
                NSString *message = [[NSString alloc] initWithString:[parseObject objectForKey:@"message"]];
                NSDate *creationDate = parseObject.createdAt;
                NSDateFormatter *df = [[NSDateFormatter alloc] init];
                [df setDateFormat:@"'Hoje, às' HH:mm"];
                NSString *createdAt = [df stringFromDate:creationDate];
                [nicknames addObject:username];
                [messages addObject:message];
                [courses addObject:userCourse];
                [messageTimes addObject:createdAt];
            }
            [self.tableView reloadData];
            [self automaticScroll];
        }
    }];
}

// Return number of courses.
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [chatRows count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Create cell
    static NSString *CellIdentifier = @"ChatTableCell";
    ChatTableCell *cell = (ChatTableCell*)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        //Faz o cache da célula para evitar criar muitos objetos desnecessários durante o scroll
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ChatTableCell" owner:self options:nil] objectAtIndex:0];
    }
    cell.username.text = [nicknames objectAtIndex:indexPath.row];
    cell.username.textColor = [UIColor whiteColor];
    cell.courseName.text = [courses objectAtIndex:indexPath.row];
    cell.courseName.textColor = [UIColor colorWithRed:(146/255.0) green:(146/255.0) blue:(146/255.0) alpha:1];
    cell.message.text = [messages objectAtIndex:indexPath.row];
    cell.message.textColor = [UIColor colorWithRed:(0/255.0) green:(122/255.0) blue:(255/255.0) alpha:1];
    cell.timeSent.text = [messageTimes objectAtIndex:indexPath.row];
    cell.timeSent.textColor = [UIColor colorWithRed:(146/255.0) green:(146/255.0) blue:(146/255.0) alpha:1];
    return cell;
}

//Method scheduled to run in loop to simulate message push
-(void)scheduledLoad {
    PFQuery *query = [[PFQuery alloc] initWithClassName:@"ChatRow"];
    [query orderByAscending:@"createdAt"];
    [query whereKey:@"createdAt" greaterThanOrEqualTo:[self getCurrentDayAt00]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível carregar as mensagens agora, verifique sua conexão e tente novamente!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        } else {
            tempChatRows = [[NSMutableArray alloc] initWithArray:objects];
            if ((chatRows != nil) && (chatRows >= 0) && (tempChatRows >= 0) && ([chatRows count] < [tempChatRows count])) {
                for (int i = [chatRows count]; i < [tempChatRows count]; i++) {
                    PFObject *parseObject = (PFObject *)[tempChatRows objectAtIndex:i];
                    [chatRows addObject:parseObject];
                    NSString *username = [[NSString alloc] initWithString:[parseObject objectForKey:@"writer"]];
                    NSString *userCourse = [[NSString alloc] initWithString:[parseObject objectForKey:@"writerCourse"]];
                    NSString *message = [[NSString alloc] initWithString:[parseObject objectForKey:@"message"]];
                    NSDate *creationDate = parseObject.createdAt;
                    NSDateFormatter *df = [[NSDateFormatter alloc] init];
                    [df setDateFormat:@"'Hoje, às' HH:mm"];
                    NSString *createdAt = [df stringFromDate:creationDate];
                    [nicknames addObject:username];
                    [messages addObject:message];
                    [courses addObject:userCourse];
                    [messageTimes addObject:createdAt];
                }
                [self.tableView reloadData];
                [self automaticScroll];
            }
        }
    }];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.messageInput resignFirstResponder];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.messageInput resignFirstResponder];
}

-(void)viewDidDisappear:(BOOL)animated {
    //Remove the scheduled execution of scheduledLoad method
    [self.timer invalidate];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.messageInput) {
        [self sendMessage:nil];
        return true;
    }
    return false;
}

-(void)automaticScroll {
    if (self.tableView.contentSize.height > self.tableView.frame.size.height) {
        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
        [self.tableView setContentOffset:offset animated:YES];
    }
}

-(NSDate *)getCurrentDayAt00 {
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now];
    [components setHour:00];
    NSDate *today00am = [calendar dateFromComponents:components];
    
    return today00am;
}

-(void)showSpinner {
    self.loadingSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.loadingSpinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)];
    [self.loadingSpinner startAnimating];
    [self.view addSubview:self.loadingSpinner];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)setUpImageBackButton {
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 22)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"backButton.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barBackButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton addTarget:self action:@selector(popCurrentViewController) forControlEvents:UIControlEventTouchUpInside];
    backButton.adjustsImageWhenHighlighted = NO;
    self.navigationItem.leftBarButtonItem = barBackButtonItem;
    self.navigationItem.hidesBackButton = YES;
}

- (void)popCurrentViewController {
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

@end
