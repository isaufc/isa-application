//
//  Map.m
//  iSA
//
//  Created by Afonso Neto on 09/06/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "Map.h"
#import "MapList.h"
#import "PlacesDAO.h"
#import "Parse/Parse.h"
#import "GoogleMaps/GoogleMaps.h"
#import "BackButton.h"

@interface Map ()

@end

@implementation Map {
    GMSMapView *mapView_;
    NSMutableArray *placesList;
    NSMutableArray *recoveredFromBDObjects;
}

@synthesize placeTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView {
    // Create a GMSCameraPosition that tells the map to display the
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-3.744806 longitude:-38.576689 zoom:15];
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.myLocationEnabled = YES;
    mapView_.mapType = kGMSTypeSatellite;
    mapView_.delegate = self;
    self.view = mapView_;
    
    placesList = [[NSMutableArray alloc] init];
    //If no place is selected, we must show all the places
    if (self.placeTitle == nil) {
        //Attributes as nil to don't filter the values, this means get all the places in BD
        [self pinMarkersWhereKColumn:nil hasValue:nil];
        self.placeTitle = nil;
    } else if (self.placeTitle) { //If have some placeId, this means the user has made a search and have selected one
        if ([self.placeTitle isEqualToString:@"Todos os lugares com Banheiro"]) {
            //Pin in map all places with Bathroom
            [self pinMarkersWhereKColumn:@"hasBathroom" hasValue:[NSNumber numberWithBool:TRUE]];
            self.placeTitle = nil;
        } else if ([self.placeTitle isEqualToString:@"Todos os lugares com Xerox"]) {
            //Pin in map all places with Xerox
            [self pinMarkersWhereKColumn:@"hasXerox" hasValue:[NSNumber numberWithBool:TRUE]];
            self.placeTitle = nil;
        } else if ([self.placeTitle isEqualToString:@"Todos os lugares com Cantina"]) {
            //Pin in map all places with Canteen
            [self pinMarkersWhereKColumn:@"hasCanteen" hasValue:[NSNumber numberWithBool:TRUE]];
            self.placeTitle = nil;
        } else if ([self.placeTitle isEqualToString:@"Todos os lugares com Bebedouro"]) {
            //Pin in map all places with Water Drinker
            [self pinMarkersWhereKColumn:@"hasWaterDrinker" hasValue:[NSNumber numberWithBool:TRUE]];
            self.placeTitle = nil;
        } else {
            //Pin in map only one place specified in self.placeTitle
            [self pinMarkersWhereKColumn:@"title" hasValue:self.placeTitle];
            self.placeTitle = nil;
        }
    }
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    NSLog(@"You tapped at %f,%f", coordinate.latitude, coordinate.longitude);
}

//Method that print in the map all the markers that is fetched from a BD search that corresponds to a value of specified column. Example: all places where column hasBathroom has value true
- (void)pinMarkersWhereKColumn:(NSString *)column hasValue:(id)value {
    //Check if the attribute values is nil and save it in BOOL var, this mean we want to show all places in the BD
    BOOL showAllPlaces = (value == nil && column == nil);
    PFQuery *query = [PFQuery queryWithClassName:@"Place"];
    if (!showAllPlaces)
        [query whereKey:column equalTo:value];
    [query orderByAscending:@"title"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível carregar os locais, verifique sua conexão com interenet e tente novamente!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        } else if (objects) {
            recoveredFromBDObjects = [[NSMutableArray alloc] initWithArray:objects];
            for (PFObject *parseObject in recoveredFromBDObjects) {
                PlacesDAO *pDao = [[PlacesDAO alloc] init];
                pDao.placeTitle = [parseObject objectForKey:@"title"];
                if (!showAllPlaces)
                    self.title = pDao.placeTitle;
                pDao.description = [parseObject objectForKey:@"description"];
                pDao.hasXerox = [[parseObject objectForKey:@"hasXerox"] boolValue];
                pDao.hasWaterDrinker = [[parseObject objectForKey:@"hasXerox"] boolValue];
                pDao.hasCanteen = [[parseObject objectForKey:@"hasCanteen"] boolValue];
                pDao.hasBathroom = [[parseObject objectForKey:@"hasBathroom"] boolValue];
                pDao.latitude = [[parseObject objectForKey:@"latitude"] doubleValue];
                pDao.longitude = [[parseObject objectForKey:@"longitude"] doubleValue];
                [placesList addObject:pDao];
            }
            
            for (PlacesDAO *pDao in placesList) {
                GMSMarker *marker = [[GMSMarker alloc] init];
                marker.position = CLLocationCoordinate2DMake(pDao.latitude, pDao.longitude);
                marker.title = pDao.placeTitle;
                marker.snippet = pDao.description;
                marker.map = mapView_;
                //If we wnt to show all place, we have to initially show all the Pici's region
                if (showAllPlaces) {
                    GMSCameraPosition *position;
                    if (mapView_.myLocation != nil && mapView_.myLocation) {
                        position = [GMSCameraPosition
                         cameraWithLatitude:mapView_.myLocation.coordinate.latitude
                         longitude:mapView_.myLocation.coordinate.longitude
                         zoom:18];
                    } else {
                        position = [GMSCameraPosition cameraWithLatitude:-3.744806 longitude:-38.576689 zoom:15];
                    }
                    mapView_.camera = position;
                } else { //If is a specified place or a specific list, center it in the last one
                    //TODO: Center in the client position obtained from GPS, else, do the bove:
                    GMSCameraPosition *position;
                    if (mapView_.myLocation != nil && mapView_.myLocation) {
                        position = [GMSCameraPosition
                                    cameraWithLatitude:mapView_.myLocation.coordinate.latitude
                                    longitude:mapView_.myLocation.coordinate.longitude
                                    zoom:18];
                    } else {
                        position = [GMSCameraPosition cameraWithLatitude:marker.position.latitude longitude:marker.position.longitude zoom:20];
                    }
                    mapView_.camera = position;
                }
            }
        }
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"PICI";
    self.navigationItem.hidesBackButton = NO;
    UIButton *btNewPost =[[UIButton alloc] init];
    [btNewPost setBackgroundImage:[UIImage imageNamed:@"btExplore.png"] forState:UIControlStateNormal];
    btNewPost.frame = CGRectMake(0, 0, 90, 30);
    UIBarButtonItem *btPost =[[UIBarButtonItem alloc] initWithCustomView:btNewPost];
    [btNewPost addTarget:self action:@selector(gotoSearchPlacesView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = btPost;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self setUpImageBackButton];
}

- (void)gotoSearchPlacesView {
    MapList *listView = [[MapList alloc] init];
    [self.navigationController pushViewController:listView animated:YES];
}

//- (void) viewWillDisappear:(BOOL)animated {
//    [mapView_ removeObserver:self forKeyPath:@"myLocation"];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
