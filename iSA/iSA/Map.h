//
//  Map.h
//  iSA
//
//  Created by Afonso Neto on 09/06/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface Map : UIViewController <GMSMapViewDelegate> {}

@property (nonatomic) NSString *placeTitle;

- (void)pinMarkersWhereKColumn:(NSString *)column hasValue:(NSString *)value;

@end
