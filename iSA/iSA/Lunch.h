//
//  Lunch.h
//  iSA
//
//  Created by Nicolau Brasil on 27/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainMealModel.h"

@interface Lunch : UIViewController
@property(nonatomic) MainMealModel *lunchToday;
@end
