//
//  Courses.m
//  iSA
//
//  Created by Nicolau Brasil on 02/06/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "Courses.h"
#import <Parse/Parse.h>
#import "CoursesDetails.h"

@interface Courses () {
    
    NSMutableArray *totalCourses;
    NSMutableArray *courseList;
    NSMutableArray *filteredCourses;
    BOOL isFiltered;
}

@end

@implementation Courses

@synthesize tableView;
@synthesize searchBar;
@synthesize loadingSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    /*self.searchDisplayController.searchResultsTableView.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(45/255.0) blue:(105/255.0) alpha:1];*/
    totalCourses = [[NSMutableArray alloc] init];
    self.searchBar.delegate = self;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self showSpinner];
    PFQuery *query = [[PFQuery alloc] initWithClassName:@"Courses"];
    [query orderByAscending:@"title"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            [self.loadingSpinner removeFromSuperview];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível carregar a lista de cursos, verifique sua conexão e tente novamente!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        } else {
            [self.loadingSpinner removeFromSuperview];
            courseList = [[NSMutableArray alloc] initWithArray:objects];
            [tableView reloadData];
            for (PFObject *parseObject in courseList) {
                NSString *titleFromBD = [[NSString alloc] initWithString:[parseObject objectForKey:@"title"]];
                [totalCourses addObject:titleFromBD];
            }
            [self.tableView reloadData];
        }
    }];
    
//    totalCourses = [[NSMutableArray alloc] initWithObjects:@"Administração", @"Agronomia", @"Arquitetura e Urbanismo", @"Biblioteconomia ", @"Biotecnologia", @"Ciências Ambientais", @"Ciências Atuariais", @"Ciências Biológicas", @"Ciências Contábeis", @"Computação", @"Ciências Econômicas", @"Ciências Sociais", @"Cinema e Audiovisual", @"Dança", @"Design", @"Design de Moda", @"Direito", @"Economia Doméstica", @"Educação Física", @"Enfermagem", @"Engenharia de Alimentos", @"Engenharia de Pesca", @"Engenharia Ambiental", @"Engenharia Civil", @"Engenharia Elétrica", @"Engenharia de Energias Renováveis", @"Engenharia Mecânica", @"Engenharia Metalúrgica", @"Engenharia de Petróleo", @"Engenharia de Produção Mecânica", @"Engenharia Química", @"Engenharia de Teleinformática", @"Estatística", @"Farmácia", @"Filosofia", @"Finanças", @"Física", @"Fisioterapia", @"Gastronomia", @"Geografia", @"Geologia", @"História", @"Jornalismo (Comunicação Social)", @"Letras", @"Letras (Espanhol - Noturno)", @"Letras (Inglês - Noturno)", @"Matemática", @"Matemática Industrial", @"Medicina", @"Música", @"Oceonografia", @"Odontologia", @"Pedagogia", @"Psicologia", @"Publicidade e Propaganda (Comunicação Social) ", @"Química", @"Secretariado Executivo", @"Sistemas e Mídias Digitais", @"Teatro", @"Zootecnia", nil];
//    
//    coursesDescription = [[NSMutableArray alloc] initWithObjects:@"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", @"Sexo", nil];
//    
//    for (int i=0; i< [totalCourses count]; i++) {
//        PFObject *courseInParse = [[PFObject alloc] initWithClassName:@"Courses"];
//        [courseInParse setObject:[coursesDescription objectAtIndex:i] forKey:@"description"];
//        [courseInParse setObject:[totalCourses objectAtIndex:i] forKey:@"title"];
//        [courseInParse saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//            if (error) {
//                NSLog(@"Deu pau!");
//            } else {
//                NSLog(@"Deu certo!");
//            }
//        }];
//    }
    
    //Needed to show the back button in the navigation bar
    self.tabBarController.navigationItem.hidesBackButton = NO;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length == 0) {
        isFiltered = NO;
    } else {
        isFiltered = YES;
        filteredCourses = [[NSMutableArray alloc] init];
        for (NSString *str in totalCourses) {
            NSRange stringRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (stringRange.location != NSNotFound) {
                [filteredCourses addObject:str];
            }
        }
    }
    [self.tableView reloadData];
}

-(void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar {
    [self.tableView resignFirstResponder];
}

// Return number of courses.
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isFiltered) {
        return [filteredCourses count];
    }
    return [totalCourses count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Create cell
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        //Faz o cache da célula para evitar criar muitos objetos desnecessários durante o scroll
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (!isFiltered) {
        cell.textLabel.text = [totalCourses objectAtIndex:indexPath.row];
    } else {
        cell.textLabel.text = [filteredCourses objectAtIndex:indexPath.row];
    }
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.accessoryView.backgroundColor = [UIColor redColor];
    tableView.separatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"separator"]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!isFiltered) {
        NSString *courseName = [totalCourses objectAtIndex:indexPath.row];
        CoursesDetails *courseDetailsView = [[CoursesDetails alloc] init];
        courseDetailsView.courseFromlistView = courseName;
        [self.navigationController pushViewController:courseDetailsView animated:YES];
    } else {
        NSString *courseName = [filteredCourses objectAtIndex:indexPath.row];
        CoursesDetails *courseDetailsView = [[CoursesDetails alloc] init];
        courseDetailsView.courseFromlistView = courseName;
        [self.navigationController pushViewController:courseDetailsView animated:YES];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.searchBar resignFirstResponder];
}

-(void)showSpinner {
    self.loadingSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.loadingSpinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)];
    [self.loadingSpinner startAnimating];
    [self.view addSubview:self.loadingSpinner];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

