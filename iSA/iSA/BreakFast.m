//
//  BreakFast.m
//  iSA
//
//  Created by Nicolau Brasil on 20/05/13.
//  Copyright (c) 2013 com.nicolaubrasil.iSA. All rights reserved.
//

#import "BreakFast.h"
#import "BreakFastModel.h"
#import "RUService.h"

@interface BreakFast ()

@end

@implementation BreakFast

@synthesize breakFastListSections;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *valueWithTheFirstHifen = [[self.breakfastToday bebidas]substringFromIndex:2];
    
    NSString *valueReplaced =[valueWithTheFirstHifen stringByReplacingOccurrencesOfString:@"-" withString:@"\n"];
    
    [self bebidas].text = valueReplaced;

    
    // Do any additional setup after loading the view from its nib.
    //    [self loadTheDay];
    //    [self loadBreakfastTitles];
    //    [self loadDaysContent:1];
}

//This method creates a array with the titles for the breakfast
//-(void)loadBreakfastTitles {
//    NSURL *DaysUrl = [NSURL URLWithString:@"http://www.ufc.br/servidores-2/restaurante-universitario/2444-cardapio-do-restaurante-universitario"];
//    NSData *RUHTMLData = [NSData dataWithContentsOfURL:DaysUrl];
//
//    TFHpple *titles = [TFHpple hppleWithHTMLData:RUHTMLData];
//
//    NSString *RUXpathQueryString = @"//table/tbody/tr/td/p/span[@style='color: #800000;']";
//    NSArray *RUTitlesNodes = [titles searchWithXPathQuery:RUXpathQueryString];
//
//    breakFastListSections = [[NSMutableArray alloc] init];
//    int count = 0;
//    for (TFHppleElement *element in RUTitlesNodes) {
//        if (count <= 3) {
//            NSString *section = [[NSString alloc] init];
//            section = [element.raw stringByReplacingOccurrencesOfString:@"<span style=\"color: #800000;\">" withString:@""];
//            section = [section stringByReplacingOccurrencesOfString:@"</span>" withString:@""];
//            NSLog(@"%@", section);
//            count++;
//        }
//    }
//}
//
//- (void)loadDaysContent:(int)dayIndex {
//    NSURL *DaysUrl = [NSURL URLWithString:@"http://www.ufc.br/servidores-2/restaurante-universitario/2444-cardapio-do-restaurante-universitario"];
//    NSData *RUHTMLData = [NSData dataWithContentsOfURL:DaysUrl];
//
//    TFHpple *titles = [TFHpple hppleWithHTMLData:RUHTMLData];
//
//    NSString *RUXpathQueryString = @"//table/tbody/tr/td/p";
//    NSArray *RUTitlesNodes = [titles searchWithXPathQuery:RUXpathQueryString];
//
//    breakFastListSections = [[NSMutableArray alloc] init];
//    int count = 0;
//    for (TFHppleElement *element in RUTitlesNodes) {
//        if (count == 0) {
//            //do nothing
//        } else if (count == dayIndex) {
//            NSString *section = [[NSString alloc] init];
//            section = [element.raw stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
//            section = [section stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
//            section = [section stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
//            section = [section stringByReplacingOccurrencesOfString:@"-" withString:@""];
//            NSLog(@"%@", section);
//        }
//        count++;
//    }
//}
//
////This method get the index for the curent day
//-(int)loadTheDay {
//    NSURL *DaysUrl = [NSURL URLWithString:@"http://www.ufc.br/servidores-2/restaurante-universitario/2444-cardapio-do-restaurante-universitario"];
//    NSData *RUHTMLData = [NSData dataWithContentsOfURL:DaysUrl];
//
//    TFHpple *titles = [TFHpple hppleWithHTMLData:RUHTMLData];
//
//    NSString *RUXpathQueryString = @"//table/tbody/tr/td/span/strong";
//    NSArray *RUTitlesNodes = [titles searchWithXPathQuery:RUXpathQueryString];
//
//    RUAuxiliar *auxObj = [[RUAuxiliar alloc] init];
//    NSString *currentDay = [[NSString alloc] initWithString:[auxObj getCurrentDay]];
//    NSLog(@"%@", currentDay);
//    currentDay = @"sexta";
//    //Test if RU is serving something today (The answers is not for Saturday and Sunday)
//    if ([currentDay caseInsensitiveCompare:@"sábado"] == NSOrderedSame || [currentDay caseInsensitiveCompare:@"domingo"] == NSOrderedSame) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Infelizmente não estamos servindo hoje." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//    int count = 0;
//    for (TFHppleElement *element in RUTitlesNodes) {
//        if (count <= 5) {
//            break;
//        }
//        if ([currentDay caseInsensitiveCompare:[[element firstChild] content]] == NSOrderedSame) {
//            //Desjejum is a trash data that is coming with the parse, só we don't need it
//            NSLog(@"%@ - %d", [[element firstChild] content], count);
//        }
//        count++;
//    }
//    return count;
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
